<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2020 http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: JaguarJack [ njphper@gmail.com ]
// +----------------------------------------------------------------------
namespace app\admin\exports;

use app\admin\model\Admin;
use app\admin\model\CatchModel;
use app\admin\support\excel\ExcelContract;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class AdminExport implements ExcelContract
{

    /**
     * 设置头部
     *
     * @return string[]
     */
    public function headers(): array
    {
        // TODO: Implement headers() method.
        return [
            'id', '用户名', '邮箱', '状态', '创建日期'
        ];
    }

    /**
     * 处理数据
     *
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function sheets(): array
    {
        // TODO: Implement sheets() method.
        $users = Admin::field(['id', 'username', 'email', 'status', 'created_at'])->select();

        foreach ($users as &$user) {
            $user->status = $user->status == CatchModel::ENABLE ? '启用' : '停用';
        }

        return $users->toArray();
    }

    /**
     * 设置开始行
     *
     * @return int
     */
    public function setRow()
    {
        return 2;
    }

    /**
     * 设置标题
     *
     * @time 2020年09月08日
     * @return array
     */
    public function setTitle()
    {
        return [
            'A1:G1', '导出用户', Alignment::HORIZONTAL_CENTER
        ];
    }
}
