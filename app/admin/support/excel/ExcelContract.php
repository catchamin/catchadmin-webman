<?php
declare(strict_types=1);

namespace app\admin\support\excel;
interface ExcelContract
{
    public function headers(): array;

    public function sheets();
}
